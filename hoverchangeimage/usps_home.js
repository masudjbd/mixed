var Surrounder = {
	elements : {
		parent : null,
		left : null,
		top : null,
		right : null,
		bottom : null
	},
	callback : null,
	initialize : function(){
		this.elements.parent = $('surround');
		this.elements.top = $('top');
		this.elements.bottom = $('bottom');
		this.elements.left = $('left');
		this.elements.right = $('right');
		this.elements.parent.addEvent('mouseover',Surrounder.hide);
	},
	el : null,
	draw : function (el, callback){
		this.callback = callback;
		this.el = el;
		var eCoord = el.getCoordinates();
		var wCoord = window.getScrollSize();
		var wCoordAlt = window.getSize();
		wCoordAlt.x = wCoordAlt.x-25;
		Surrounder.elements.top.setStyles({
			'height':eCoord.top,
			'width':wCoordAlt.x
		});
		Surrounder.elements.bottom.setStyles({
			'height':wCoord.y-(eCoord.top+eCoord.height),
			'top':eCoord.top+eCoord.height,
			'width':wCoordAlt.x
		});
		Surrounder.elements.left.setStyles({
			'height':eCoord.height,
			'top':eCoord.top,
			'width':eCoord.left
		});
		Surrounder.elements.right.setStyles({
			'height':eCoord.height,
			'top':eCoord.top,
			'width':(wCoordAlt.x - (eCoord.width+eCoord.left)).limit(0,99999),
			'left':(eCoord.width+eCoord.left)
		});
		Surrounder.elements.parent.setStyle("display","block");
	},
	hide : function(){
		Surrounder.elements.parent.setStyle("display","none");
		Surrounder.callback(Surrounder.el);
	}
}

var Hover = {
	initialize : function(){
		$$('.hover').each(function(el,index){
			el.addEvent('mouseover',function(){
				this.setStyle('background-position','-'+this.getSize().x+'px'+' 0px');
			});
			el.addEvent('mouseout',function(){
				this.setStyle('background-position','0px 0px');
			});
		});
	}
}

window.addEvent('domready',function(){
	Surrounder.initialize();
	Hover.initialize();
});
