var Steps = {
	currElement : null,
	settings : {
		shippingClass : 'shipping-step',
		shippingHoverClass : 'shipping-step-over',
		shippingInfo : '.shipping-info',
		shippingBkg : '.shipping-step-bkg',
		shippingNumber : '.shipping-step-number',
		shippingNumberOffset : 85,
		shippingArrowWidth : 36,
		shippingArrowHeight : 39,
		shippingArrow : 'shippingArrow'
	},
	initialize : function(){
		$$('.'+this.settings.shippingClass).each(
			function (el,index){
				el.getElement(Steps.settings.shippingBkg).setStyle('opacity','0');
				el.addEvent('mouseenter', function(){
					Steps.animate(el);
				});				
			}
		);
	},
	animate : function(el){
		Surrounder.draw(el,Steps.revert);
		el.getElement(Steps.settings.shippingInfo).setStyles({
			'opacity':'0',
			'overflow':'auto',
			'height':'auto',
			'width':'auto'
		});		
		el.getElement(Steps.settings.shippingInfo).fade('in');
		el.getElement(Steps.settings.shippingBkg).tween('opacity',0.9);
		el.getElement(Steps.settings.shippingNumber).tween('bottom',Steps.settings.shippingNumberOffset);
		var id = el.id.substr(12,1);
		if (id<=3){
			$(Steps.settings.shippingArrow+id).morph({
				'width':Steps.settings.shippingArrowWidth,
				'height':Steps.settings.shippingArrowHeight
			});
		}
	},
	revert : function(el){
		el.getElement(Steps.settings.shippingInfo).fade('out');
		el.getElement(Steps.settings.shippingBkg).fade('out');
		el.getElement(Steps.settings.shippingNumber).tween('bottom',0-(el.getElement(Steps.settings.shippingNumber).hasClass('step3')?4:0));
		var id = el.id.substr(12,1);
		if (id<=3){
			$(Steps.settings.shippingArrow+id).morph({
				'width':0,
				'height':0
			});
		}
	}
}


window.addEvent('domready',function(){
	Steps.initialize();
});
