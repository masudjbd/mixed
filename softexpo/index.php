<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SoftExpo Expertise Review</title>
<style type="text/css">
body{ background-image:url(images/appl_bg.jpg); background-position:center; background-repeat:no-repeat; background-attachment:fixed;}
table{ font-family:Arial, Helvetica, sans-serif; font-size:12px;}
#roundleft{ background-image:url(images/roundbox-top.png); background-position:bottom; background-repeat:no-repeat;}
#roundmiddle{ background-image:url(images/roundbox-middle.png); background-position: center; background-repeat:repeat-y; width:580px;}
#roundbottom{ background-image:url(images/roundbox-bottom.png); background-position:top; background-repeat:no-repeat;}
#apptitle{ font-size:18px; font-weight:bold; color:#FFF; background-color:#0099FF;}
.tfield{ padding:2px; border:1px solid #CCCCFF; background:#FFFFFF;}
.resetbtn{ width:131px; height:47px; border:none;  background-image:url(images/clearbtn.jpg); background-position:center; background-repeat:no-repeat;}
span{ color:#6600FF;}
h1{ font-size:25px; color:#FF0099;}
center{ padding-top:150px;}
.lmenuex{padding:3px; border:1px solid #CCCCFF; background:#FFFFFF;}
.lmenu{padding:2px; border:1px solid #CCCCFF; background:#FFFFFF; font-size:11px;}
#ash{ background-color:#F6F6F6; background-image:url(images/tfldbg.jpg); background-repeat:repeat-y; }
#gry{ background-color:#EEEEEE; background-image:url(images/tfldbgl.jpg); background-repeat:repeat-y;}
#exptitle{ background-color:#C8E4F9;}
</style>
<script language="javascript" type="text/javascript">
function batti()
{
	if(document.appl.others.checked)
	{ document.getElementById('otherdiv').innerHTML = '<input type="text" name="othr" id="othr" size="60" value="" />'; }
	else { document.getElementById('otherdiv').innerHTML = ''; }
}

function formval()
{
	if(document.appl.name.value == ''){ document.getElementById('nameerr').innerHTML = 'Please provide your name.';	return false;}
	else {document.getElementById('nameerr').innerHTML = '';} 
	
	if(document.appl.org.value & document.appl.des.value == ''){ document.getElementById('deserr').innerHTML = 'Please provide your designation.';	return false; }
	else { document.getElementById('orgerr').innerHTML = ''; }
	
	if(document.appl.contact.value == '') { document.getElementById('contacterr').innerHTML = 'Please provide your contact no.'; return false; }
	else { document.getElementById('contacterr').innerHTML = ''; }
	
	if(document.appl.email.value == '')	{ document.getElementById('emailerr').innerHTML = 'Please provide your email id.'; return false; }
	else { document.getElementById('emailerr').innerHTML = ''; } 
	
return true; 
}
</script>
</head>

<body>
<?php 
if($_POST['submit'] == 'sent')
{
	$name = $_POST['name'];
	$org = $_POST['org'];
	$des = $_POST['des'];if(!$_POST['des']){$des == '';}
	$contact = $_POST['contact'];
	$email = $_POST['email'];
	$experience = $_POST['experience'];
	//echo $experience;
	$interest = $_POST['interest'];if(!$_POST['interest']){$interest == '';}
	$like = $_POST['like'];	if(!$_POST['like']){$like == 'No';}
	
	//expertise collect
	$expertise = '';
	$comma = ''; 
	if($_POST['osource']){	
		$expertise .= $_POST['osource'].' = '.$_POST['osourceexp'];
	}
	
	if($_POST['dotnet']){
		if($expertise != '')
			$comma = ', ';
		$expertise .= $comma.$_POST['dotnet'].' = '.$_POST['osourceexp'];
	}
	
	if($_POST['3dsmax']){
		if($expertise != '')
			$comma = ', ';
		$expertise .= $comma.$_POST['3dsmax'].' = '.$_POST['osourceexp'];
	}
	
	$phpmysql = $_POST['phpmysql'];
	if($_POST['phpmysql']){$expertise .= ', '.$phpmysql.' = '.$_POST['osourceexp'];}
	$ror = $_POST['ror'];if($_POST['ror']){$expertise .= ', '.$ror.' = '.$_POST['osourceexp'];}
	$flash =$_POST['flash'];if($_POST['flash']){$expertise .= ', '.$flash.' = '.$_POST['flashexp'];}
	$asc =$_POST['asc'];if($_POST['asc']){$expertise .= ', '.$asc.' = '.$_POST['ascexp'];}
	$ajax =$_POST['ajax'];if($_POST['ajax']){$expertise .= ', '.$ajax.' = '.$_POST['ajaxexp'];}
	$xml =$_POST['xml'];if($_POST['xml']){$expertise .= ', '.$xml.' = '.$_POST['xmlexp'];}
	$xhtml =$_POST['xhtml'];if($_POST['xhtml']){$expertise .= ', '.$xhtml.' = '.$_POST['xhtmlexp'];}
	$css =$_POST['css'];if($_POST['css']){$expertise .= ', '.$css.' = '.$_POST['cssexp'];}
	$cplus =$_POST['cplus'];if($_POST['cplus']){$expertise .= ', '.$cplus.' = '.$_POST['cplusexp'];}
	$csharp =$_POST['csharp'];if($_POST['csharp']){$expertise .= ', '.$csharp.' = '.$_POST['csharpexp'];}
	$multimedia =$_POST['multimedia'];if($_POST['multimedia']){$expertise .= ', '.$multimedia.' = '.$_POST['multimediaexp'];}
	$cms =$_POST['cms'];if($_POST['cms']){$expertise .= ', '.$cms.' = '.$_POST['cmsexp'];}
	$seo =$_POST['seo'];if($_POST['seo']){$expertise .= ', '.$seo.' = '.$_POST['seoexp'];}
	$oracle =$_POST['oracle'];if($_POST['oracle']){$expertise .= ', '.$oracle.' = '.$_POST['oracleexp'];}
	$postgre =$_POST['postgre'];if($_POST['postgre']){$expertise .= ', '.$postgre.' = '.$_POST['postgreexp'];}
	$ldap =$_POST['ldap'];if($_POST['ldap']){$expertise .= ', '.$ldap.' = '.$_POST['ldapexp'];}
	$python =$_POST['python'];if($_POST['python']){$expertise .= ', '.$python.' = '.$_POST['pythonexp'];}
	$coldfusion =$_POST['coldfusion'];if($_POST['coldfusion']){$expertise .= ', '.$coldfusion.' = '.$_POST['coldfusionexp'];}
	$jsp =$_POST['jsp'];if($_POST['jsp']){$expertise .= ', '.$jsp.' = '.$_POST['jspexp'];}
	$perl =$_POST['perl'];if($_POST['perl']){$expertise .= ', '.$perl.' = '.$_POST['perlexp'];}
	$java =$_POST['java'];if($_POST['java']){$expertise .= ', '.$java.' = '.$_POST['javaexp'];}
	$systemadmin =$_POST['systemadmin'];if($_POST['systemadmin']){$expertise .= ', '.$systemadmin.' = '.$_POST['systemadminexp'];}
	$iphone =$_POST['iphone'];if($_POST['iphone']){$expertise .= ', '.$iphone.' = '.$_POST['iphoneexp'];}
	$business =$_POST['business'];if($_POST['business']){$expertise .= ', '.$business.' = '.$_POST['businessexp'];}
	$animation =$_POST['animation'];if($_POST['animation']){$expertise .= ', '.$animation.' = '.$_POST['animationexp'];}
	$creative =$_POST['creative'];if($_POST['creative']){$expertise .= ', '.$creative.' = '.$_POST['creativeexp'];}
	$othr =$_POST['othr'];if($_POST['othr']){$expertise .= ', '.$othr;}
	$customersupport =$_POST['customersupport'];if($_POST['customersupport']){$expertise .= ', '.$customersupport.' = '.$_POST['customersupportexp'];}
	
	//expr
	include ("admin/admin_conn.php");
	$sql = "insert into expo (name,org,des,contact,email,interest,expert,experience) values ('".$name."','".$org."','".$des."','".$contact."','".$email."','".$interest."','".$expertise."','".$experience."')";
	//echo $sql;
	$success = mysql_query($sql);
	//if($success){ echo "<h1>Thanks for your submission.</h1>";}
}

if($success){ 
?>
<table width="580" border="0" cellspacing="0" align="center" cellpadding="0">
  <tr>
    <td id="roundleft">&nbsp;</td>
  </tr>
  <tr>
    <td id="roundmiddle" align="center">
	<table width="580" border="0" align="center" style="background-image:url(images/formbg.jpg); background-position:center top; background-repeat:repeat-x;" cellpadding="3" cellspacing="0">
			  <tr>
			    <td>&nbsp;</td>
			    <td colspan="2">&nbsp;</td>
	      </tr>
			  <tr>
			    <td colspan="3" id="apptitle" align="center">&nbsp;</td>
	      </tr>
			  <tr>
			    <td colspan="3" align="center">
						<img src="images/thanks-msg.jpg" style="padding-top:150px; padding-bottom:200px;" border="0" alt="Thanks for your submission" />
				</td>
	      </tr>
		  </table>
	</td>
  </tr>
  <tr>
    <td id="roundbottom">&nbsp;</td>
  </tr>
</table>
<?php 
//echo "<center><h1>Thanks for your submission.</h1></center>";
echo '<meta http-equiv="refresh" content="15;url=index.php" /> ';
}
else {
?>
<form name="appl" action="<?php $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" onsubmit="return formval(this);">
<table width="580" border="0" cellspacing="0" align="center" cellpadding="0">
  <tr>
    <td id="roundleft">&nbsp;</td>
  </tr>
  <tr>
    <td id="roundmiddle" align="center">
		<table width="580" border="0" align="center" style="background-image:url(images/formbg.jpg); background-position:center top; background-repeat:repeat-x;" cellpadding="3" cellspacing="0">
			  <tr>
			    <td>&nbsp;</td>
			    <td colspan="2">&nbsp;</td>
	      </tr>
			  <tr>
			    <td colspan="3" id="apptitle" align="center">Soft Expo. Review Appl.</td>
	      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td colspan="2">&nbsp;</td>
	      </tr>
			  <tr>
				<td width="161" valign="top"><strong>&nbsp;&nbsp;Name : </strong></td>
				<td colspan="2" valign="top">
				    <input name="name" type="text" id="name" class="tfield" size="40" /><br />
				<span id="nameerr"></span>	</td>
			  </tr>
			  <tr>
				<td valign="top"><strong>&nbsp;&nbsp;Organization/Individual</strong></td>
				<td colspan="2" valign="top"><input name="org" type="text" id="org" class="tfield"size="40" />		<br />
			<span id="orgerr"></span>				</td>
			  </tr>
			  <tr>
                <td valign="top"><strong>&nbsp;&nbsp;Designation</strong></td>
			    <td colspan="2" valign="top"><input name="des" type="text" id="des" class="tfield" size="40" /><br />	<span id="deserr"></span>			</td>
	      </tr>
			  <tr>
				<td valign="top"><strong>&nbsp;&nbsp;Contact Info</strong></td>
				<td colspan="2" valign="top"><input name="contact" type="text" id="contact" class="tfield" size="40" /><br />
			<span id="contacterr"></span>			</td>
			  </tr>
			  <tr>
				<td valign="top"><strong>&nbsp;&nbsp;E-mail :</strong></td>
				<td colspan="2" valign="top"><input name="email" type="text" id="email" class="tfield" size="40" /><br />
				<span id="emailerr"></span>				</td>
			  </tr>
			  <tr>
				<td valign="top"><strong>&nbsp;&nbsp;Area of Interest</strong></td>
				<td colspan="2" valign="top"><input name="interest" type="text" id="interest" class="tfield" value="Programming" onfocus="if(this.value == 'Programming'){this.value='';}"  onblur="if(this.value == ''){this.value='Programming';}" size="40" /></td>
			  </tr>
			  <tr>
			    <td valign="top"><strong>&nbsp;&nbsp;Job Experience </strong></td>
			    <td colspan="2" valign="top">
				<select name="experience" id="experience" class="lmenuex">
			      <option value="1" selected="selected">1 Year</option>
			      <option value="2">2 Years</option>
			      <option value="3">3 Years</option>
			      <option value="4">4 Years</option>
			      <option value="5">5 Years</option>
			      <option value="6+">6 +</option>
				</select>			    </td>
	      </tr>
			  <tr>
				<td valign="top"><strong>&nbsp;&nbsp;Expertise</strong></td>
				<td colspan="2" valign="top">&nbsp;</td>
			  </tr>
			  
			  <tr>
			    <td valign="top"><strong>&nbsp;&nbsp;Skills achievement on : </strong></td>
			    <td colspan="2" valign="top">&nbsp;</td>
	      </tr>
		  <tr>
		  	<td colspan="3" align="center">
				<table width="550" border="0" cellspacing="0" cellpadding="0">
				  
				   <tr>
				     <td height="25" align="left" id="exptitle">&nbsp;&nbsp;<strong>Skill on.</strong></td>
				     <td align="left" id="exptitle">&nbsp;&nbsp;<strong> Experience</strong> </td>
				     <td align="left" id="exptitle">&nbsp;&nbsp;<strong>Skill on. </strong></td>
				     <td align="left" id="exptitle">&nbsp;&nbsp;<strong>Experience</strong> </td>
			      </tr>
				   <tr>
						<td width="146" align="left" id="ash"><label><input type="checkbox" name="osource" value="Open Source" />Open Source .</label></td>
						<td width="121" align="left" id="ash"><select name="osourceexp" id="osourceexp" class="lmenu">
							  <option value="1" selected="selected">1 Year</option>
							  <option value="2">2 Years</option>
							  <option value="3">3 Years</option>
							  <option value="4">4 Years</option>
							  <option value="5">5 Years</option>
							  <option value="6+">6 +</option>
							</select>						</td>
						<td width="192" align="left" id="ash">
							<label><input type="checkbox" name="dotnet" value="Dot NET " />.NET </label>						</td>
						<td width="91" align="left" id="ash">
						<select name="dotnetexp" id="dotnetexp" class="lmenu">
							  <option value="1" selected="selected">1 Year</option>
							  <option value="2">2 Years</option>
							  <option value="3">3 Years</option>
							  <option value="4">4 Years</option>
							  <option value="5">5 Years</option>
							  <option value="6+">6 +</option>
						  </select>						</td>
			  	</tr>
				  <tr>
					<td id="gry"><label>
					  <input type="checkbox" name="phpmysql"  value="PHP / MySQL"  /> PHP / MySQL</label></td>
					<td id="gry"><select name="phpmysqlexp" id="phpmysqlexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="gry"><label>
					  <input type="checkbox" name="css"  value="CSS" />
					  CSS </label>
                      <label></label></td>
					<td id="gry"><select name="cssexp" id="cssexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="ash"><label>
					  <input type="checkbox" name="ror"  value="Ruby on Rails"  />
					  Ruby on Rails. </label>
                      <label></label></td>
					<td id="ash"><select name="rorexp" id="rorexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="ash"><label>
					  <input type="checkbox" name="cplus"  value="C/C++ " />C/C++ </label></td>
					<td id="ash"><select name="cplusexp" id="cplusexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="gry"><label>
					  <input type="checkbox" name="flash"  value="Flash"  />
					  Flash. </label>
                      <label></label></td>
					<td id="gry"><select name="flashexp" id="flashexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="gry"><input type="checkbox" name="csharp" value="C Sharp "  />
				    C #</td>
					<td id="gry"><select name="csharpexp" id="csharpexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="ash"><input type="checkbox" name="asc" value="Action Script "  />
				    Action Script. (2/3)</td>
					<td id="ash"><select name="ascexp" id="ascexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="ash"><input type="checkbox" name="multimedia"  value="Multimedia" />
				    Multimedia</td>
					<td id="ash"><select name="multimediaexp" id="multimediaexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="gry"><input type="checkbox" name="ajax" value="Ajax "  />
				    Ajax</td>
					<td id="gry"><select name="ajaxexp" id="ajaxexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="gry"><label>
					  <input type="checkbox" name="cms"  value="CMS" />
					  CMS . </label>
                      <label></label></td>
					<td id="gry"><select name="cmsexp" id="cmsexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="ash"><label>
					  <input type="checkbox" name="xml" value="XML "  />
					  XML</label>
                      <label></label></td>
					<td id="ash"><select name="xmlexp" id="xmlexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="ash"><input type="checkbox" name="seo"  value="Search Engine Optimization" />
				    Search Engine Optimization .</td>
					<td id="ash"><select name="seoexp" id="seoexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="gry"><input type="checkbox" name="xhtml" value="XHTML"  />
				    XHTML</td>
					<td id="gry"><select name="xhtmlexp" id="xhtmlexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="gry"><label>
					  <input type="checkbox" name="oracle" value="Oracle"  />
					  Oracle. </label>
                      <label></label></td>
					<td id="gry"><select name="oracleexp" id="oracleexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="ash"><input type="checkbox" name="3dsmax" value="3ds Max " />
				    3ds Max</td>
					<td id="ash"><select name="3dsmaxexp" id="3dsmaxexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="ash"><input type="checkbox" name="ldap"  value="LDAP " />
				    LDAP .</td>
					<td id="ash"><select name="ldapexp" id="ldapexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="gry"><input type="checkbox" name="postgre" value="PostgreSQL "  />
				    PostgreSQL .</td>
					<td id="gry"><select name="postgreexp" id="postgreexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="gry"><input type="checkbox" name="python"  value="Python" />
				    Python .</td>
					<td id="gry"><select name="pythonexp" id="pythonexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="ash"><input type="checkbox" name="coldfusion"  value="Cold Fusion" />
				    Cold Fusion.</td>
					<td id="ash"><select name="coldfusionexp" id="coldfusionexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="ash"><label>
					  <input type="checkbox" name="jsp" value="JSP "  />
					  JSP . </label>
                      <label></label></td>
					<td id="ash"><select name="jspexp" id="jspexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="gry"><label>
					  <input type="checkbox" name="perl" value="Perl/CGI"  />
					  Perl/CGI . </label></td>
					<td id="gry"><select name="perlexp" id="perlexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="gry"><input type="checkbox" name="java"  value="Java" />
				    Java .</td>
					<td id="gry"><select name="javaexp" id="javaexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="ash"><label>
					  <input type="checkbox" name="iphone" value="iPhone"  />
					  iPhone. </label>
                      <label></label></td>
					<td id="ash"><select name="iphoneexp" id="iphoneexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="ash"><input type="checkbox" name="systemadmin" value="System Administration"  />
				    System Administration .</td>
					<td id="ash"><select name="systemadminexp" id="systemadminexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="gry"><input type="checkbox" name="business"  value="Business Assistant" />
				    Business Assistant .</td>
					<td id="gry"><select name="businessexp" id="businessexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="gry"><label>
					  <input type="checkbox" name="graphics"  value="Graphics Design" />
					  Graphics Design </label>
                      <label></label></td>
					<td id="gry"><select name="graphicsexp" id="graphicsexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="ash"><input type="checkbox" name="creative"  value="Creative Writing" />
				    Creative Writing .</td>
					<td id="ash"><select name="creativeexp" id="creativeexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="ash"><label>
					  <input type="checkbox" name="animation"  value="Animation" />
					  Animation. </label>
                      <label></label></td>
					<td id="ash"><select name="animationexp" id="animationexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
				  </tr>
				  <tr>
					<td id="gry"><input type="checkbox" name="customersupport"  value="Customer Support" />
				    Customer Support.</td>
					<td id="gry"><select name="customersupportexp" id="customersupportexp" class="lmenu">
                      <option value="1" selected="selected">1 Year</option>
                      <option value="2">2 Years</option>
                      <option value="3">3 Years</option>
                      <option value="4">4 Years</option>
                      <option value="5">5 Years</option>
                      <option value="6+">6 +</option>
                    </select></td>
					<td id="gry">
						<input type="checkbox" name="others" id="others" onclick="batti();"  value="" />Others					</td>
					<td id="gry">&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan="4" id="ash">
					<div id="otherdiv"></div>					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
		    </table>			</td>
		  </tr>
			  <tr>
			    <td colspan="3" valign="middle">&nbsp;&nbsp;<input type="checkbox" name="like" value="yes" />
would you like to market your marketing North America ? </td>
	      </tr>
			  <tr>
			    <td colspan="3">&nbsp;</td>
	      </tr>
			  <tr>
			    <td>&nbsp;</td>
	            <td width="250" align="right" valign="middle">
				<input type="image" src="images/submitbtn.jpg" name="submit" value="sent" />				</td>
		        <td width="145" valign="middle" align="left">
				<input type="reset" class="resetbtn" name="Reset" value="" />				</td>
		  </tr>
	  </table>

		
	</td>
  </tr>
  <tr>
    <td id="roundbottom">&nbsp;</td>
  </tr>
</table>
</form>
<?php } ?>
</body>
</html>
