<?php
session_start();
	include("common/check_login.php");
	
	if($_REQUEST['msg'] == 1)
		$Success_Message = "Password changed successfully";
	else if($_REQUEST['msg'] == 2)
		$Error_Message = "Sorry!!! System is unable to change password";
	else if($_REQUEST['msg'] == 3)
		$Error_Message = "Please fill up all field.";
	else if($_REQUEST['msg'] == 4)
		$Error_Message = "Password do not match. Please try again.";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.. Sina Developer .. </title>
<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		theme_advanced_buttons1 : "save,bold,italic,underline,|,cut,copy,pastetext,|,forecolor,backcolor",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		content_css : "css/content.css",
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<link href="common/style.css" rel="stylesheet" type="text/css" />
</head>
<body class="twoColElsLtHdr">
<div id="container">
<table width="920" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td colspan="2" valign="middle" height="100"><?php include "common/header.php"; ?></td>
		  </tr>
		  <tr><td height="8" colspan="2" id="hbottomb"></td></tr>
		  <tr>
			<td width="230" height="420" align="left" valign="top" style="border-right:1px solid #666666;">
				<?php include "common/left_menu.php"; ?>
			</td>
			<td width="700" align="left" valign="top">
  <div id="mainContent">
	  <fieldset style="width:450px;">
	  	<legend>Change Password</legend>
    	   <font color="#009900"><?php echo $Success_Message;?></font><font color="#FF0000"><?php echo $Error_Message;?></font>
    <p>
    	<table width="100%" cellpadding="5" cellspacing="0" border="0">
        <form method="post" action="change_password_code.php">
        	<tr>
            	<td width="35%">
					<font size="2">Old Password:</font>
				</td>
                <td><input type="password" name="Old_Password" id="Old_Password" size="20" /></td>
            </tr>
            <tr>
            	<td><font size="2">New Password:</font></td>
                <td><input type="password" name="New_Password" id="New_Password" size="20" maxlength="12" /></td>
            </tr>
            <tr>
            	<td><font size="2">Retype New Password:</font></td>
                <td><input type="password" name="New_Password2" id="New_Password2" size="20" maxlength="12" /></td>
            </tr>
            <tr>
            	<td></td>
                <td><input type="submit" class="submitbutton" name="submit" value="Change Password" /></td>
            </tr>
    	</form>
        </table>
    </p>
	  </fieldset>
    </div>
</td>
		  </tr>
		  <tr>
			<td colspan="2">

   <?php include("common/footer.php");?>
</td>
		  </tr>
		</table>


</div>
</body>
</html>