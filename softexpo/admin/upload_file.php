<?php
session_start();
	include("common/check_login.php");
	
	if($_REQUEST['msg'] == 1)
		$Success_Message = "File Uploded Successfully";
	else if($_REQUEST['msg'] == 2)
		$Error_Message = "Sorry!!! System is unable to Upload File";
	else if($_REQUEST['msg'] == 3)
		$Error_Message = "Please select file then try.";
	else if($_REQUEST['msg'] == 4)
		$Error_Message = "This Image is already exist try to upload by another file name.";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.. Sina Developer .. </title>
<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		theme_advanced_buttons1 : "save,bold,italic,underline,|,cut,copy,pastetext,|,forecolor,backcolor",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<link href="common/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style3 {
	color: #FF3399;
	font-weight: bold;
}
-->
</style>
</head>

<body class="twoColElsLtHdr">
<table align="center" width="920" border="0" style="border:1px solid #999999;" cellspacing="0" cellpadding="0">
  	<tr>
    	<td bgcolor="#FFFFFF" height="100">
			<?php include "common/header.php"; ?>
		</td>
  	</tr>
	<tr><td height="8" colspan="2" id="hbottomb"></td></tr>
  	<tr>
    	<td bgcolor="#FFFFFF">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="230" align="left" valign="top" style="border-right:1px solid #666666;">
						<?php include "common/left_menu.php"; ?>
					</td>
					<td width="700" align="left" valign="top" height="420">
						<h2>&nbsp;Add File </h2>
						
						<p>
						<script language="javascript">
						function fval()
						{
							if(document.fupload.fileName.value==''){
							alert('Please provide a file name');
							return false;
							}
							if(document.fupload.uploaded_file.value==''){
							alert('Please select a file to upload');
							return false;
							}
							
							return true;
						}
						</script>
							<form method="post" name="fupload" action="upload_file_code.php" enctype="multipart/form-data"
							 onsubmit="return fval(this);">            
								<table cellpadding="5" cellspacing="0" width="85%">
									<tr>
										<td align="center" valign="middle"> &nbsp; </td>
										<td><span class="resultmsg">
												&nbsp;<?php echo $Success_Message;?>
												&nbsp;<?php echo $Error_Message;?>
											</span>
										</td>
									</tr>
									<tr>
									  <td width="20%" align="left" valign="top" class="pfont">&nbsp;File Name:</td>
									  <td width="80%" align="left" valign="top" class="pfont">						
									  		<input type="text" name="fileName" class="select250css">
									  </td>
									</tr>
									<tr>
									  <td width="20%" align="left" valign="top" class="pfont">&nbsp;File Description:</td>
									  <td width="80%" align="left" valign="top" class="pfont">						
									  	<textarea name="fileDes" cols="30" rows="4"></textarea>
									  </td>
									</tr>
									<tr>
										<td align="left" valign="top" class="pfont">&nbsp;Upload Photo:</td>
										<td align="left" valign="top" class="pfont">
											<input type="file" name="uploaded_file"><br />
												<span class="style3">!NB: </span>
													You must have to upload your file maximum 500MB.									  </td>
									</tr>
									<tr>
										<td colspan="2" align="center" class="pfont">
											<br />
											<input type="submit" name="submit" value=" Proceed to Upload " class="submitbutton" />&nbsp;&nbsp;
											<input type="reset" name="reset" value=" Reset " class="submitbutton" />
									  </td>
									</tr>
							  </table>
							</form>
						</p>
					</td>
				</tr>
			</table>
		</td>
  	</tr>
  	<tr>
    	<td>
			 <?php include("common/footer.php");?>
		</td>
  	</tr>
</table>
</body>
</html>
