<?php
session_start();
	require_once("admin_conn.php");
	include("common/check_login.php");
	if($_REQUEST['msg'] == 1)
		$Success_Message = "file updated successfully";
	else if($_REQUEST['msg'] == 2)
		$Error_Message = "Sorry!!! System is unable to update file";
	else if($_REQUEST['msg'] == 3)
		$Error_Message = "Please write file Name.";
	else if($_REQUEST['msg'] == 4)
		$Success_Message = "File Remove Successfully";
	else if($_REQUEST['msg'] == 5)
		$Error_Message = "Unable to Remove File";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin Panel</title>
<script LANGUAGE="JavaScript">
function confirmSubmit()
{
	var agree=confirm("Are you sure you want to remove this file?");
	if (agree)
		return true ;
	else
		return false ;
}
// -->
</script>

<link href="common/style.css" rel="stylesheet" type="text/css" />
</head>
<body class="twoColElsLtHdr">
<table width="920" style=" border:1px solid #CCCCCC;" border="0" cellspacing="0" align="center" cellpadding="0">
		  <tr>
			<td colspan="2" valign="middle" bgcolor="#FFFFFF" height="100">
			<?php include "common/header.php"; ?>
		</td>
		  </tr>
		  <tr><td height="8" colspan="2" id="hbottomb"></td></tr>
		  <tr>
			<td width="230" align="left" valign="top" style=" background-color:#FFFFFF;border-right:1px solid #666666;">
						<?php include "common/left_menu.php"; ?>
					</td>
					<td width="700" align="left" valign="top" height="420" bgcolor="#FFFFFF">
						<h2>&nbsp;&nbsp; File List </h2>
									<span class="resultmsg">
										&nbsp;<?php echo $Success_Message;?>
										&nbsp;<?php echo $Error_Message;?>
									</span>
							<?php
							//$product_id = $_POST['product_id'];
							$sql = "SELECT * FROM u_n_d order by f_date desc";
							$query = mysql_query($sql);
							$No_Of_Rows = mysql_num_rows($query);
							if($No_Of_Rows>0)
							{?>
								<table cellpadding="5" align="center" cellspacing="0" border="0" bordercolordark="#CCCCCC"
								 width="85%">
									<tr>
										<td bgcolor="#CCCCCC" class="pfont"><strong>File Name:</strong></td>
										<td bgcolor="#CCCCCC" class="pfont"><strong>File Size</strong></td>
										<td bgcolor="#CCCCCC" class="pfont"><strong></strong></td>
										<td bgcolor="#CCCCCC" class="pfont">&nbsp;</td>
										<td bgcolor="#CCCCCC" class="pfont"><strong>Remove</strong></td>
									</tr>
									<?php
									while($row = mysql_fetch_row($query))
									{
									?>
										<tr>
											<td bgcolor="#F2F2F2" class="pfont" style="border-bottom:1px solid #CCCCCC;">
												<?php echo $row[1]; ?>
										    </td>
											<td height="22" bgcolor="#F2F2F2" class="pfont" 
											style="border-bottom:1px solid #CCCCCC;">
												<?php echo $row[4].'&nbsp;Byte'; ?>
											</td>						
											<td bgcolor="#F2F2F2" class="pfont" style="border-bottom:1px solid #CCCCCC;">&nbsp;
													
										  </td>						
											<td bgcolor="#F2F2F2" class="pfont" style="border-bottom:1px solid #CCCCCC;">
												
													<?php /*?><form name="" method="post" action="#">
													<input type="hidden" name="id" value="<?php echo $row[0];?>" />
													<input class="submitbutton" type="submit" name="submit" value=" Edit " />
												</form>	<?php */?>	
											</td>
											<td bgcolor="#F2F2F2" class="pfont" style="border-bottom:1px solid #CCCCCC;">						
												<form name="" method="post" action="remove_file_code.php" 
												onsubmit="return confirmSubmit()">
													<input type="hidden" name="id" value="<?php echo $row[0];?>" />
													<input class="submitbutton" type="submit" name="submit" value="Remove" />
												</form>
										 </td>
										</tr>
									<?php	
									}
								?>
					  </table>
							<?php	
							}
							else
							{
								echo "No file found for download";
							}	
						?>
					</td>
				</tr>
				<tr><td colspan="2"> <?php include("common/footer.php");?></td></tr>
			</table>
	
		
</body>
</html>
