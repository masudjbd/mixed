<?php
$past = time() - 100;
//this makes the time in the past to destroy the cookie
setcookie(ID_my_site, gone, $past);
setcookie(Key_my_site, gone, $past);
header("Location: index.php");
exit;

/*
	All our logout page does is destroy the cookie, and then direct them back to the login page. We destroy the cookie by setting the expiration to some time in the past.
*/
?>
